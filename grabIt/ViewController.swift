//
//  ViewController.swift
//  grabIt
//
//  Created by Kasim Hussain on 13/11/2017.
//  Copyright © 2017 Kasim Hussain. All rights reserved.
//

import UIKit
import AWSS3

class ViewController: UIViewController, UIImagePickerControllerDelegate,UINavigationControllerDelegate {
    
    @IBOutlet weak var imageView: UIImageView!
    @IBOutlet weak var backgroundView: UIImageView!
    @IBOutlet weak var saveButton: UIButton!
    @IBOutlet weak var statusLabel: UILabel!
    
    let picker = UIImagePickerController()
    
    var selectedImage: UIImage!{
        didSet{
            saveButton.isEnabled = true
}
        
    }
    override func viewDidLoad() {
        super.viewDidLoad()
        picker.delegate = self
    }
    
    @IBAction func photoLibrary(_ sender: Any) {
        picker.allowsEditing = false
        picker.sourceType = .photoLibrary
        picker.mediaTypes = UIImagePickerController.availableMediaTypes(for: .photoLibrary)!
        present(picker, animated: true, completion: nil)
    }
    
    @IBAction func saveButton(_ sender: Any) {
        statusLabel.text = "Uploading..."
        guard let image = imageView.image else {return}
        if let data = UIImagePNGRepresentation(image){
            DispatchQueue.main.async(execute: {
                let transferUtility = AWSS3TransferUtility.default()
                let S3BucketName = "myphotostore"
                let expression = AWSS3TransferUtilityUploadExpression()
                let accessKey = ""
                let secretKey = ""
                let credentialsProvider = AWSStaticCredentialsProvider(accessKey: accessKey, secretKey: secretKey)
                let configuration = AWSServiceConfiguration(region: AWSRegionType.EUWest2, credentialsProvider: credentialsProvider)
                AWSServiceManager.default().defaultServiceConfiguration = configuration
                
                func randomString(_ length: Int) -> String {
                    
                    let master = Array("abcdefghijklmnopqrstuvwxyz-ABCDEFGHIJKLMNOPQRSTUVWXYZ_123456789".characters) //0...62 = 63
                    var randomString = ""
                    
                    for _ in 1...length{
                        
                        let random = arc4random_uniform(UInt32(master.count))
                        randomString.append(String(master[Int(random)]))
                    }
                    return randomString
                }
                
                transferUtility.uploadData(data, bucket: S3BucketName, key: randomString(64) , contentType: "image/png", expression: expression) { (task, error) in
                    if let error = error{
                        print(error.localizedDescription)
                        return
                    }
                    self.statusLabel.text = "Uploaded Successfully!"
                }
            })
            
        }
        
    }
    func imagePickerController(_ picker: UIImagePickerController, didFinishPickingMediaWithInfo info: [String : Any]) {
        
        if let image = (info[UIImagePickerControllerOriginalImage] as? UIImage){
            selectedImage = image
            imageView.image = selectedImage
            // backgroundView.image = imageView.image
        }
        dismiss(animated:true, completion: nil)
    }
    func imagePickerControllerDidCancel(_ picker: UIImagePickerController) {
        dismiss(animated: true, completion: nil)
    }
}
